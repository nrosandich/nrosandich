## Hi :wave: I'm Nate Rosandich and I am an Engineering Manager at Gitlab working with the [SSCS::Compliance group](https://about.gitlab.com/handbook/engineering/development/sec/govern/compliance/). 

I began my career as a fullstack developer, before moving into people leadership and enjoyed the challenge of solving problems through/with others. My role as an EM is to build engaged and empowered teams that will make great decisions and produce great solutions. I do not have all the answers but I love working with people to discover the solution. Outside of work I spend most of my time with my family, exploring nature or on the slopes skiing and mountain biking.

### My Values
Below is a list of my personal values and how I try to use them to guide my decisions.

- **Quality** - Striving for quality over quantity in all things, whether this is code, discussions or 1:1s. Quality will ensure that we are getting results faster in the long run. This is not always the easiest route.
- **Transparency** - Being open and honest allows everyone to make the correct decisions. Trust over micromanagement
- **Simplicity** - Look for the simplest solution first and iterate quickly. Dont over complicate
- **Iteration** - Fast and constructive feedback, using data over opinion to make informed decisions. Ensure no surprises.
- **Empathy** - Everyone also has lives outside of work, understanding this is important as a leader. People over process. Time off the computer is key to navigate complex problems.
- **Learn in the open** - Being prepared to fail and learn from it. Experimentation over assumptions

I try to reflect on how I use these guidelines by reflecting on my completed todos. Generally as part of a todo I will make a decision, but this decision is usually done in the moment. At the end of the week I collate my completed todos/decisions and compare them against these guidelines, looking for opportunities to improve my decision making.

### My Role:

**EM Role Breakdown**

I like to consider the EM role broken down into 4 pillars (4Ps) as documented by [Pat Kua](https://www.patkua.com/blog/5-engineering-manager-archetypes/) and [Thiago Ghisi](https://www.linkedin.com/pulse/how-build-strong-career-tech-thiago-ghisi/). I believe that the EM role takes on all four of these core pillars at any one time, but in different percentages as the situation requires. While not necessarily the decision maker, an EM does need context and ability in all 4 areas to ask the right questions.

- _Platform_ (Codebase, Engineering Practices, Architecture Patterns & New Technologies)
- _Product_ (Project Management, Continuous Delivery, User Experience & Product Roadmap)
- _Process_ (Culture, Metrics, Rituals & Systems)
- _People_ (Mentorship, Feedback, Leadership Behaviours & Career Growth Strategy),

**1:1s**

I consider 1:1s to be my most important meetings as a manager. This time is specifically for my team to discuss their concerns / ideas and how we can progress themselves professionally and the team. 

I like to schedule at least a weekly 25 minute meeting. I like to use a standing agenda similar to examples in the [handbook](https://about.gitlab.com/handbook/leadership/1-1/), this is so we can both contribute to it asynchronously before discussing outcomes in the meeting.

Because empathy is one of my personal values I also like to use this time to get to know my team personally. My team does not have to discuss their personal lives with me, but I do find understanding someone personally and their situation helps to provide valuable context to their work and decision making. 

As this meeting is for and primarily about my team, I am open to changing how it is run, what we discuss, how often and when.

**Feedback** 

TBD

### Gotchas
- This is not an exhaustive document on me
- This document might and will change without notice.
- MRs welcome with any updates
